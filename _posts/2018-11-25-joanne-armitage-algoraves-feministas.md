---
layout: post
categories: posts
title: "Joanne Armitage en Algoraves feministas"
---

# Joanne Armitage en Algoraves feministas
*original [https://www.lullabot.com/podcasts/hacking-culture/joanne-armitage-on-feminist-algorave](https://www.lullabot.com/podcasts/hacking-culture/joanne-armitage-on-feminist-algorave)*

traducción por
- rapo
- munshkr
- torotumbo



Matthew Tift:
Es noviembre de 2018, episodio 18. Joanne Armitage sobre algoraves feministas.

Matthew Tift:
Mi invitada de hoy es la Doctora Joanne Armitage, conferencista sobre digital media en la Universidad de Leeds. Sus areas de investigación incluyen computación física, estudios científicos y tecnológicos, y música computacional. Ella participa regularmente en algoraves como parte del duo de live coding, ALGOBABEZ, con Shelly Knotts. Ambas vienen, segun su sitio web, "rompiendo tímpanos con incorregible algo-pop industrial impulsado por sintetizadores desde 2016". Joanne es parte de OFFAL, la Orquesta Para Mujeres y Computadoras. Recientemente ganó el Premio Daphne Oram de la Asociación de Ciencia Británica a la Innovación Digital. Pienso que el trabajo que ella está haciendo es importante e innovador, y nunca antes conocí a Joanne, y me complace tenerla en el programa y tener esta oportunidad para descubrir más. Muchas gracias por participar en Hacking Culture, Joanne.

Joanne Armitage:
Hola Matthew. Muchas gracias por invitarme. Estoy realmente emocianada de hablar con vos.

Matthew Tift:
Yo también. Creo que, en general, el esquema que tenía en mente era hablar un poco sobre tus antecedentes, después tal vez hablar un poco sobre las algoraves y el live coding en general. Y luego quizás hablar un poco sobre algunos de los problemas de la tecnología y de la música, y algunas de las cosas que estas haciendo para ayudar en estos problemas. Eso era en general lo que tenía en mi cabeza, pero podemos ir a donde vos quieras.

Joanne Armitage:
Sí, eso suena bien. Sí.

Matthew Tift:
Genial. Intenté juntar tu historia en base a algunos artículos, videos y sitios web diferentes. ¿Tengo razón al decir que tenés un doctorado en "música de computadora"?

Joanne Armitage:
Si, supongo que es bastante de música de computadora. Se basó en el Centro Interdisciplinario de Investigación Científica en Música, que ya no es un centro de investigación activo. En realidad es donde me encontré con Alex McLean, que es un live coder bastante conocido. Ese fue mi doctorado, y creo que en su mayor parte cae en los dominios de la computación musical.

Matthew Tift:
Y eso quizás no sea algo de lo que mucha gente haya oído hablar. ¿Eso es más un título en música, o más como un título en programación?

Joanne Armitage:
Sí, se trataba de un doctorado basado en la práctica, así que en su mayor parte se trata de hacer cosas, pero no de evaluarlas. Hacer cosas para experimentarlas, y luego iterarlas y rehacerlas. Así que creo que aunque hubo un nivel de ingeniería y computación involucrado, definitivamente fue una especie de doctorado en artes o doctorado en música.

Matthew Tift:
Oh, interesante. Ahora eres profesora de Nuevos Medios en la Universidad de Leeds, ¿es correcto?

Joanne Armitage:
Si, si. Así que realmente he estado en esta posición aproximadamente desde el último año de mi doctorado, y terminé dando el salto a la enseñanza de los medios en un módulo de proyecto para apoyar proyectos digitales. Terminé quedándome allí, y he estado ahí durante unos cuatro años. Mi rol está creciendo un poco. Ahora contextualizo mi trabajo mucho más dentro de los medios digitales, desde dentro de la música, pero todavía trato de mantener vivo un poco de ese trabajo académico a través de mis colaboraciones con Shelly.

Matthew Tift:
Seguro.

Joanne Armitage:
Mm-hmm (afirmando).

Matthew Tift:
Y Shelly siendo la otra mitad de ALGOBABEZ.

Joanne Armitage:
Si, Shelly Knotts.

Matthew Tift:
Ese es todo un nombre. Parece que ustedes tocan por todo el mundo.

Joanne Armitage:
Sí, creo que es solo el nombre que lo hace por nosotros, de verdad. Me estaba llamando ALGOBABE por un tiempo, y cuando Shelly me pidió tocar conmigo, le compartí el nombre y ella le puso una Z al final.

Matthew Tift:
¿Dijiste en algún momento que pensás que podrías ser una de las dos primeras mujeres involucradas en el live coding?

Joanne Armitage:
No estoy segura. Creo que en Europa Shelly Knotts y Norah Lorway fueron las primeras personas con las que realmente me encontré performeando. Llegué un poco más tarde. Pero por un tiempo, fuimos solo nosotras dos actuando regularmente en la escena del Reino Unido, tal vez por mas de un año.

Matthew Tift:
Ya veo, pero de cualquier manera, definitivamente estás entre las primeras mujeres involucradas en este movimiento y ahora realmente ha crecido bastante. Supongo que eso es una especie de transición. Parece que las algoraves y el movimiento de live coding han crecido bastante alrededor de donde vivís. ¿Podrías darnos... y sé que esta es una pregunta difícil de alguna manera, pero una pequeña definición de lo que pensas sobre algorave o el live coding, o la conexión entre los dos?

Joanne Armitage:
Sí. Para mí, el live coding es solo un uso exploratorio de la programación en el escenario. Es algo que es performativo, y algo que es incierto, y algo que está incorporado y algo que involucra escuchar, reaccionar y muchas relaciones complejas con el entorno. Así que para mi, el live coding es eso. Supongo que algorave es una especie de público que se enfrenta al live coding, donde se vuelve un poco más socializado y un poco más de fiesta.

Matthew Tift:
Claro, ¿entonces las algoraves son un lugar donde la gente hace live coding en público o con otras personas?

Joanne Armitage:
Si, si. Definitivamente.

Matthew Tift:
Dirías que todos los conciertos de live coding son algoraves, o hay otros tipos de eventos de live coding que no son algoraves?

Joanne Armitage:
Sí, definitivamente no. El live coding tiene su origen en una especie de cosas que se extiende de la música electroacústica, por lo que existen entornos mucho más experimentales en salas de conciertos que en lugar de clubes. Así que creo que las algoraves son generalmente más un ambiente de fiesta y el live coding es simplemente un término más amplio, como esas actuaciones electroacústicas que pueden entrar en la definicion.

Matthew Tift:
Esta bien. Otro grupo del que eres parte es OFFAL

Joanne Armitage:
Si, OFFAL

Matthew Tift:
OFFAL. Ya veo, como awful (n.t. OFFAL se pronuncia casi igual a awful, que significa terrible, en el buen o mal sentido). No me dí cuenta.

Joanne Armitage:
Sí. Se trata de un nombre terriblemente bueno (n.t. juego de palabras)

Matthew Tift:
Mm-hmm (afirmando). Pero eso sería más como un grupo que realiza live coding, pero no necesariamente en una situación de algorave.

Joanne Armitage:
Si, como que OFFAL no es tan algorave. De hecho, no sé si OFFAL podría participar en un algorave, porque realmente luchamos con la incriminación del ritmo, así que creo que hacerlo bailable sería un gran desafío computacional. Por lo general, tocamos sets tipo drone, idealmente en sistemas de parlantes multicanal, para que todos puedan tener la posición y el espacio de sonido.

Joanne Armitage:
Utilizamos instrucciones y lenguajes de live coding de alto nivel para live coding. Es un poco como algo entre la notación y el código, aunque se podría argumentar que son algo muy similar de todos modos, para instruirse entre sí y tener sistemas de votación para implementar cambios en el sonido. Todo es un poco más sobre tipo de textura, supongo. Textura y sonoridad en lugar de ritmo.

Matthew Tift:
Ah, bueno, esa es una buena manera de decirlo.

Joanne Armitage:
Mm-hmm (afirmando).

Matthew Tift:
Entonces, ¿es OFFAL un grupo que se presenta en persona o leí en alguna parte que eso también sucede en línea?

Joanne Armitage:
Sí, la mayoría de la banda se une de forma remota. Hemos tenido algunos shows donde ha habido cinco intérpretes IRL (n.t. in real life, es decir, en persona), que ha sido bastante especial. En Canadá, creo que fue a finales de 2016, hicimos una presentación juntos y había cinco personas en la sala, y algunas de nosotras no nos habíamos conocido y eso fue realmente especial. Pero a menudo la gente está en todo el mundo y hay medios bastantes distantes allí, y por eso la gente transmite en vivo, que luego unimos y reproducimos desde SuperCollider.

Matthew Tift:
Ah, ¿entonces todos ustedes están usando SuperCollider?

Joanne Armitage:
No, la gente trabaja en diferentes plataformas. Algunas personas están haciendo una especie de electrónica en vivo con Max/MSP y violines. Otras están haciendo voces procesadas. Pero todos están transmitiendo audio, que luego capturamos... Shelly Knotts escribió el software que lo une en SuperCollider y luego se reproduce a través del sistema de sonido.

Matthew Tift:
Ya veo.

Joanne Armitage:
Mm-hmm (afirmando).

Matthew Tift:
Así que hay muchas formas diferentes de cómo la gente hace live coding. Parece que muchos de ellos terminan enviando señales a SuperCollider que luego ayuda con la transferencia para hacer su sonido.

Joanne Armitage:
Sí, es que mucha síntesis se hace en SuperCollider. De hecho, la mayoría de los lenguajes, desde Tidal a FoxDot a Ixilang, a Conductive, todos regresan a SuperCollider en el extremo que suena.

Matthew Tift:
Te entiendo. Probablemente parezca un poco básico, pero creo que mucha gente no está familiarizada con live coding. Sólo en seis meses de experiencia con él he encontrado que generalmente confunde a las personas, y se necesita un tiempo para que la gente tenga una idea de lo que estoy hablando con live coding. Así que, en cierto sentido, estoy haciendo estas preguntas de una manera muy básica porque las he escuchado varias veces, como estoy seguro de que tú también.

Joanne Armitage:
Totalmente, y encuentro esto todo el tiempo, especialmente para explicar lo que hago a familiares. Siempre es bastante divertido, o como en reuniones inocuas con personas que preguntan: "Oh, ¿qué haces?" Oh, ahí vamos.

Matthew Tift:
¿Y tienes una respuesta preparada para esa pregunta, o te lleva un tiempo explicarlo?

Joanne Armitage:
Normalmente sólo digo que soy una música electrónica, y trato de dejarlo ahí en lo posible. Pero si necesito explicar live coding, sí, doy quizás una definición similar a la que te di, o digo "codeo en vivo", que parece un poco extraño, o "hago música de laptop". Si las personas está realmente interesadas, profundizo, y siempre trato de sacar mi computadora portátil y hacer una demostración si puedo.

Matthew Tift:
Sí. Creo que dijiste en una charla en diciembre del 2017 que todavía no hemos descubierto una buena definición para live coding. ¿Crees que eso sigue siendo cierto un año más tarde?

Joanne Armitage:
Creo que el live coding se está convirtiendo en... a medida que crece, se vuelve más complejo y difícil de precisar. Así que no sé si se volvió más fácil. Puede ser que se haya vuelto más difícil de definir. Creo que la gente tiene muchas definiciones y entendimientos diferentes de esto. Pienso que es algo que la gente habla mucho. Hay una adición general sobre algorave, pero quizás con suerte... bueno, contribuí algo a esto.

Joanne Armitage:
Creo que saldrá a finales de año, pero espero que haya avances con algunas de estas preguntas. Quiero decir que son preguntas que han sido formuladas durante mucho tiempo. Creo que la dificultad de definir live coding es algo que también es realmente bueno, y algo que significa que todavía está vivo de alguna manera.

Matthew Tift:
Sí, esa es una buena manera de decirlo. Claramente tenes experiencia hablando de esto.

Joanne Armitage:
Sí, tengo mucha experiencia en mi cabeza, en persona a través de estas cosas. También trato de hablar con muchas personas diferentes para obtener distintas perspectivas. O sea, live coders, y pensadores, y personas de diferentes prácticas, y personas que tienen diferentes experiencias y conocimientos, y un tipo diferente de antecedentes, tanto como sea posible.

Matthew Tift:
Claro.

Joanne Armitage:
Mm-hmm (afirmando).

Matthew Tift:
Y lo veremos en tan solo un momento, pero supongo que otra forma en la que lo expresaste es que live coding es como una actividad ludita. No es necesariamente algo nuevo. También tengo que mencionar que esto fue en respuesta a algunos comentarios a un artículo en The Guardian que tenía una foto y un video tuyos, pienso que muy útiles, que apareció en varios lugares. Como un breve resumen de cinco minutos.

Matthew Tift:
Entonces The Guardian hizo este artículo, y luego algunas personas respondieron y sugirieron que live coding es algo nuevo, pero tu lo describiste como una clase de actividad ludita, o algo así. ¿Sigue siendo cierto?

Joanne Armitage:
Sí, ese es el tipo de idea que he acuñado de Alex McLean, pero creo que tal vez otras personas también lo han dicho. Creo que es una manera realmente útil de pensar en live coding, especialmente cuando a menudo se lo describe o se lo experimenta como algo realmente hiper-técnico. Y es realmente fácil de sobre-tecnicalizar... No sé si eso es una palabra, pero me refiero al acto de la programación y la performance del código.

Joanne Armitage:
Creo que es realmente interesante retomar esas conversaciones un poco y decir: "En realidad, esto no es tan futurista como parece". Y tal vez eso sea un problema con la forma en que la programación es vista en la sociedad y como es ubicada en ciertos espacios y no en otros. Ese es el problema. Creo que es una manera realmente útil de provocar ese tipo de pensamiento.

Matthew Tift:
Sí. Parece ser una actividad única que no encaja muy bien con solo programar o hacer música, o una combinación de ambas, o simplemente... es mucho más que una teoría. Es un verdadero tipo de actividad práctica. Pero esta noción de que todas estas personas diferentes se unen para un evento como una algorave, como resultado terminás con muchos doctores, muchos músicos y mucha gente que solo quiere bailar música, y luego gente curiosa, y todo lo demás en el medio.

Joanne Armitage:
Sí, creo que muchas de las personas que conozco que realmente se involucraron en ek live coding y tocaron, han podido obsesionarse con eso de alguna manera. Ya sea a través de un doctorado, o a través de una obsesión con el sonido, o la necesidad de una obsesión y la necesidad de algo en que trabajar. Encontras muchos doctores en una algorave. Creo que tal vez tenga que ver con el tiempo. Pero en realidad, es probable que tenga más que ver con las redes, y cómo se transfiere y comparte el conocimiento. Llegué a algorave porque conocía dos doctores, algunos doctores con... Si una escena comienza en la comunidad académica, tendrá que trabajar muy duro para salir de ella.

Matthew Tift:
¿Crees que el live coding es una especie de empresa fundamentalmente anti-comercial? ¿Está más orientado a la academia o cualquier otro lugar que no sea para ganar dinero?

Joanne Armitage:
Sí, creo que muchos... quiero decir, se usa software libre, y eso obviamente es una posición en contra de la comercialización. Pero creo que esto es algo que tal vez veremos cambiar con el tiempo. Personalmente, hace poco hice un taller para un gimnasio en Londres, dirigido por Adidas, así que tal vez aparezcan algunos elementos de comercialización. En realidad, fue un concierto muy divertido. Hice un taller de programación en un estudio de yoga. No fue muy comercial. No tenía una sensación muy comercial, por lo que realmente lo disfruté y lo pasé muy bien. Creo que... lo siento, ¿podemos volver a tu pregunta?

Matthew Tift:
Mi pregunta tenía que ver con si es justo describir live coding como anti-comercial.

Joanne Armitage:
Sí, sí. Creo que es anti-comercial y es instigación. Pero al mismo tiempo, sí, dependía de las instituciones académicas. A medida que sale de la academia y se está saliendo de la academia, otras instituciones deben ingresar y complementarla financieramente para que sea sostenible. Sé que Alex McLean en el Reino Unido tiene fondos del Consejo de las Artes de Inglaterra (Arts Council England), que es como un fondo nacional para las artes. Sé que el entorno de financiación es muy diferente de donde te encontrás, lamentablemente. Pero hay un fondo nacional en el Reino Unido para el Consejo de las Artes. Las personas obtienen fondos y apoyo institucional para organizar eventos.

Matthew Tift:
Me sorprendió cuando vi tantos eventos de live coding que fueron financiados por alguna organización artística, y no solo personas que se juntan al azar. Da la sensación de que hay algo más en juego cuando tienes un Consejo de las Artes que financia este tipo de cosas.

Joanne Armitage:
Sí, creo que el Consejo de las Artes de Inglaterra está muy, muy interesado en proyectos digitales. Realmente quieren apoyar proyectos digitales. Entonces, una algorave puede parecer algo bueno para apoyar, ya que cubre muy bien las bases en términos de artes y tecnologías. Pero creo que también ha habido algún patrocinio comercial de algoraves, que no ha sido tan bueno en mi experiencia. Tuvimos a una empresa de tecnología que les preguntaba a las personas acerca de sus A-levels, que es el examen final de preparatoria en el Reino Unido. Unos treintañeros son una gran cosa para preguntar sobre sus niveles de A y obtener empleos en tecnología. Fue un poco raro y cursi. Logré evitar ser entrevistada. Ha habido algunos patrocinios e intereses corporativos. Creo que la gente a veces consigue fechas, para tratar de obtener un poco de... pueden recibir un buen pago, pueden ser sorprendentemente terriblemente pagados, y siempre es bastante agradable rechazar uno de esos shows.

Matthew Tift:
Interesante. Eso es un poco fascinante porque la comunidad con la que estoy más familiarizado, la comunidad de Drupal, pasó, o ha estado atravesando, por lo menos diría que durante la última década, una transición de personas que usaban Drupal por diversión a usarlo para trabajar. He investigado mucho sobre los números, he publicado algunos artículos sobre la influencia corporativa y ahora más de la mitad de las personas de la comunidad que contribuyen son patrocinadas, generalmente, por la empresa o alguien más que los financia.

Matthew Tift:
Pero con el live coding, una de las cosas que lo hace tan atractivo es que aparenta ser una actividad que es inherentemente menos fácil de convertir en una carrera, digamos. Tal vez algunas personas lo están haciendo, pero en general eso, en cierto sentido, fue una de las cosas que me atrajeron, esta forma de hacerlo por diversión.

Joanne Armitage:
Sí, creo que eso es muy interesante. Hace unos años di una charla en una conferencia en la Universidad de Lancaster llamada Griswold. Y alguien ahí, después de mi charla, me dijo: "Oh, bueno, parece que vas a tener una carrera interesante en live coding". Me impresionó mucho porque nunca pensé que lo haría... simplemente no es algo que considerás cuando empezás a tocar música con código. Pero será una carrera. Lo que encontré interesante es que gran parte de mi trabajo no estaba realmente centrado en el live coding. Cuando comencé a hacer live coding, la gente estaba mucho más interesada en eso que en mi otro trabajo.

Joanne Armitage:
Así que realmente se convirtió en el centro de atención de lo que hago. Está realmente integrado en mi carrera, pero creo que se debe a que la naturaleza del live coding y el tipo de ética que tiene están realmente integrados en cómo pienso la tecnología y mi relación con la tecnología y cómo enseño la tecnología. Entonces, de alguna manera, tal vez tenga una carrera en live coding, pero tal vez no de una manera tan directa como alguien que realiza live coding o tiene sponsors para ser un live coder. Pero no estoy segura si realmente quiero eso, porque pienso que la performance del live coding es... hacer performances de live coding todo el tiempo sería bastante difícil.

Matthew Tift:
Ah, y puede ser un poco menos divertido, también.

Joanne Armitage:
Sí, quizás. A veces, cuando la agenda se vuelve muy pesada, empieza a sentirse menos divertido. Puede que me ponga un poco gruñona.

Matthew Tift:
Bueno, supongo que si alguien está más en eso... yo llego a hacer mucho como live coder. Parece que estás en ese pequeño grupo de personas que realizan actuaciones de live coding con regularidad. No creo que haya mucha gente así en el mundo por lo que puedo decir. ¿Crees que es correcto?

Joanne Armitage:
Sí, he sido muy afortunada al haber estado en muchos conciertos geniales recientemente, y me siento muy privilegiada de que me soliciten para viajar por muchas instituciones diferentes, ya sean festivales o universidades, o en ocasiones solo organizaciones artísticas más pequeñas. Voy a ir a Japón el jueves, lo cual es muy emocionante, con Alex McLean y Lucy Cheesman, tambien conocida como Heavy Lifting. Sí, tengo la oportunidad de ir a muchos shows realmente buenos. Lo consigo, volviendo al lado financiero, y conseguí... logré obtener algunos ingresos, pero es muy poco confiable y desigual. No sé si "los ingresos" es el término correcto. Creo que suena como dinero de bolsillo, compensando mi trabajo precario.

Matthew Tift:
Parece que hay alguna conexión con eso, y las personas que simplemente quieren pasar un buen rato los fines de semana y tocar en un bar o algo así. Aunque, toda la cultura en sí misma parece mucho más introspectiva, y analítica, y académica.

Joanne Armitage:
Es totalmente académico, sí. Hacer que la gente piense demasiado sobre por qué están haciendo las cosas. Creo que muchos live coders son así de todas formas, y son personas realmente reflexivas y confiadas. Para hacer una amplia generalización sobre la comunidad en la que estoy involucrado.

Matthew Tift:
¿Podrías hablar un poco acerca de las algoraves feministas y los intentos de involucrar a más mujeres en el live coding?

Joanne Armitage:
Sí, creo que una de mis mayores contribuciones, esta escena en la que comparto una posición con muchas otras mujeres como Shelly Knotts, que ya hemos mencionado, y Lucy Cheesman y Miri Kat; ellas están trabajando hacia una mayor diversidad. Para mí, sí, ese ha sido un factor muy importante y motivador para hacer lo que hago. Ya ha tenido algunos cambios, y se siente especial haber conocido a tantas mujeres increíbles en ese proceso, ya sea que terminen como programadoras de giras de live coding a nivel internacional o de solo haber venido a un taller. Realmente no ha cambiado su práctica o lo que están haciendo, pero puedes verlas haciendo cosas increíbles.

Joanne Armitage:
Hay un video del taller que Shelly y yo hicimos, que fue el primer taller de mujeres. Ella lo hizo hace poco en su charla en México. Yo estaba un poco cansada y emocionada, no en un sentido de embriaguez, mas de conferencia; y casi estaba llorando ante ese pedazo de video porque durante el tiempo que se filmó, realmente no conocía muy bien a mucha gente de esa habitacion. Pero mirándolo ahora hacia atrás, dos años después, vi una sala de personas que ahora son mis amigos y me hizo emocionar al ver que a veces simplemente hacer una pequeña cosa puede cambiar por completo las redes y el área en la que una está trabajando, explorando.

Matthew Tift:
Ese es un muy buen video, y definitivamente pondré un enlace en las notas del programa. ¿Ese taller que hiciste con esas mujeres, es el único taller solo para mujeres que realizaste? ¿O hiciste otros talleres como ese, que son solo para mujeres?

Joanne Armitage:
Sí, he hecho bastantes talleres para mujeres y personas no binarias en el Reino Unido. He trabajado con Siren, que es un colectivo de DJs con sede en Londres, para hacer un workshop. En Japón, estaremos haciendo algunos talleres solo para mujeres. Sí, creo que hay algunos otros que quizás se me están escapando, pero sí hemos hecho algunos talleres especiales. Sí.

Matthew Tift:
Parece que también hacés otros talleres. Parece que el fin de semana pasado estabas haciendo un taller al que llamaste ejercicios de desaprendizaje, y que lo hiciste un día en una casa comunitaria y al otro día en un ayuntamiento. ¿Podrías contarnos un poco sobre ese taller?

Joanne Armitage:
Sí, ese taller fue con una amiga muy cercana y colaboradora mía, Gretta Eacott, que es una sorprendente percusionista de Copenhague. Es en colaboración con este colectivo con sede en Londres. Así que estabamos intentando hacer talleres lúdicos con ejercicios y prácticas de aprendizaje ligeramente diferentes, y explorar código y percusión, y el tipo de conexion y espacio entre ellos. Queríamos realmente ir a lo rural... no a lo rural, pero estaban en ciudades, entonces tal vez fuera de lo común en términos de comunidades musicales experimentales. Creo que ambos estaban en... dos estaban en centros espaciales comunitarios y uno en el Town Hall. El Town Hall fue un poco más bajo en cuanto a los números de lo que habíamos anticipado, pero tuvimos una gran experiencia. Así que personas de muchas edades diferentes y muchas experiencias diferentes participaron. Fue financiado por Sound and Music, una organización en el Reino Unido que patrocina nuevos proyectos musicales. Así que estábamos haciendo cosas como cajas de ritmos, pero con instrumentos acústicos. Y luego en una cuadrícula las personas ponían objetos y definían un cierto patrón de batería y luego lo convertían en código.

Joanne Armitage:
Entonces, tratar de traer algo que sea realmente tangible como la cuadrícula, hacer que la gente realice eso y luego decir: "Bueno, si anotamos eso en la computadora" y cómo o cuáles son las posibilidades de hacerlo, para traducir entre los dos. Acabo de bajarme. Así que fue... terminamos el domingo, así que realmente no me ha llegado bien, pero fue un proceso muy divertido en el que estoy ansiosa por reflexionar. Será un poco más en un par de semanas. Estoy mirando mis notas y los registros que hicimos, y tal vez hagamos una evaluación y probablemente agreguemos una publicación en el blog sobre eso.

Matthew Tift:
Genial. Esa descripción de donde estás codeando y estás teniendo a alguien que está haciendo percusión. Parece que es otra parte de la experiencia del live coding que tal vez me sorprendió un poco, y podría ser un poco más difícil de explicar porque tenes a alguien que trabaja en una computadora con alguien que quizás esté tocando un tambor. Ese tipo de colaboración parece ser otra característica habitual de los eventos de live coding.

Joanne Armitage:
Sí. Siento que no es tan regular como debería ser. Me encantaría que fuera más regular. Pero definitivamente es algo que la gente hace. Lo he hecho. Greta y yo hemos actuado juntas y siempre es un desafío la sincronización, y siempre hay problemas... Hemos tenido que elegir antes con la computadora que controla el reloj. La última vez que tocamos juntas, me sentí como un tap tempo, por lo que Gretta pudo cambiar el ritmo un poco, lo que lo hizo un poco más una relación bidireccional en términos de cómo decidimos qué tan rápido o lento tocar.

Joanne Armitage:
Lucy Cheesman ha estado tocando con bandas, lo que ha sido muy divertido de ver. Creo que Miri Kat ha estado tocando con un sintetizador modular. Un sinte modular, un ser humano real. Me encanta ver ese tipo de colaboraciones, pero sí a veces... Veo a Alex McLean y Matthew Yee-King tocando como Canute, lo que es una gran colaboración. Sería genial ver más este tipo de cosas. Creo que es muy bueno tener diferentes sonidos y libertades.

Matthew Tift:
Sí, ciertamente tomaría un poco más de planificación que simplemente sentarse frente a tu computadora y jugar con el código.

Joanne Armitage:
Sí, sí. Y un poco más de práctica también, que el live coding es notoriamente malo en eso.

Matthew Tift:
En la practica?

Joanne Armitage:
Muchos de los live coders que conozco. Yo incluida, no soy muy buena practicando. Para ser honesta, mi estudio, me acabo de mudar de casa y mi estudio está hecho pedazos en este momento. Ni siquiera puedo entrar en la habitación.

Matthew Tift:
Tu workshop reciente, al que llamaste Los ejercicios de desaprendizaje, me recordó todas las cosas que pensé sobre las computadoras, la programación y otras nociones de que el live coding me ha hecho repensar porque pensé en la música de cierta manera y ahora gracias al live coding lo pienso de otra manera. Pensé en la programación de una manera determinada, como algo que se verifica y tiene calidad asegurada, y para crear una cosa utilizable y usable, pero ahora el live coding ha cambiado eso. Entonces, ¿eso es parte de lo que estás tratando con ese título de Desaprendizaje de Ejercicios, porque estás desaprendiendo tus ideas preconcebidas?

Joanne Armitage:
Sí, en realidad no se me ocurrió el título. Era algo que... el proyecto existía antes de que me involucrara, y se centraba principalmente en el tema de la percusión, porque viene con ese espíritu, entonces realmente pienso en las cosas de manera un poco diferente y se presentan las cosas en nuevas formas, y cómo es útil aprender, explorar y tocar con conceptos musicales. Pero eso se traduce, realmente, bien en incorporar live coding en él porque es una especie de desaprender, desaprender y luego volver a aprender, y luego desaprender de nuevo, y la naturaleza de las cosas como la iteración y ese tipo de metáforas que venían muy bien con esa idea.

Matthew Tift:
Eso me recuerda otra idea acerca del live coding y algo que dijiste sobre live coding como un espacio seguro para fallar/errar, y la idea de que las personas podrían participar en una algorave o en un taller, como los que han dirigido, y sentir que están en un espacio seguro para fallar. Me hizo preguntarme qué tan difícil es convencer a alguien de que un espacio es seguro, porque realmente parece que muchas de las personas en la comunidad de live coding son abiertas y acogedoras, y no tienen esas ideas preconcebidas sobre que tenés que crear esta hermosa pieza de música o estás fracasando. ¿Cómo ha sido eso, enseñar en todos estos talleres y convencer a la gente de que es un espacio seguro para fallar?

Joanne Armitage:
Sí, es realmente interesante lidiar con el fracaso en el live coding, y particularmente en los talleres, los talleres más feministas que he realizado están relacionados con todo tipo de cosas sobre el conocimiento tecnológico y la propiedad de la experiencia. A veces me emociono un poco en los talleres, y a menudo me dejo llevar un poco y termino gritandole a las personas, pidiéndoles que fracasen por mí. Siento que tanto Shelly como yo realmente hemos impulsado esta narrativa. En el momento en que tenemos una diapositiva en nuestra presentación que es solo un montón de errores en la pantalla y Shelly sosteniendo una copa de vino, y comentando: "Fallando, y bebiendo por esto".

Joanne Armitage:
Entonces realmente intentamos impulsar esta idea de fallar, y fallar como parte de un proceso creativo. Eso no es una idea nueva. Es algo que la gente ha sacado a relucir en cosas como el diseño. Pero tal vez en la computación, la falla y el error a menudo se tratan como algo que es un poco más rígido. Nos gusta hablar de las diferentes formas en que podemos fallar. Así que podemos fallar porque ha habido un problema con el hardware de audio o el proyector, ese es un tipo de falla que puede estar fuera del espacio de la computadora portátil. O podríamos haber fallado porque hemos cometido un error en nuestro código, un error de sintaxis. Podemos fallar porque hemos hecho un sonido que simplemente no funciona muy bien.

Joanne Armitage:
Hay otras formas en que podemos fallar, que no puedo pensar en este momento. Pero sí, hay muchas formas de fallar en el live coding y creo que la inestabilidad es lo que realmente me impulsó a desafiarme a mí misma para desarrollarme y comenzar a jugar más y más. Creo que fallar es realmente difícil, y algo de lo que me he vuelto más consciente recientemente es pedirle a la gente que falle, pero pedirle a la gente que falle es bastante difícil. Es algo que no todos se sienten cómodos haciendo. He estado haciendo esto durante bastante tiempo, así que me parece gracioso... quiero decir que puedo encontrar el fracaso muy frustrante, pero en ese momento puedo mirar hacia atrás, reírme y volarlo.

Joanne Armitage:
Pero para algunas personas hay mucho más en juego. Creo que las narrativas de equilibrio son nuestros fracasos, y es gracioso y divertido. Pero también es algo frustrante, desempoderado y vergonzoso. Creo que poder mirar hacia atrás después de un evento es una forma de manejarlo. Creo que solo se trata de crear un ambiente divertido donde todos están fallando juntos y sea divertido, y nadie esté solo haciendo las cosas mal. Todos estamos compartiendo esa experiencia y volviendo a hacerlo, y así otra vez. Frecuentemente fracaso en mis propios talleres de diferentes maneras.

Matthew Tift:
Estas trabajando con computadoras.

Joanne Armitage:
Si, "ayer andaba". Es una frase comun.

Matthew Tift:
El resto del mundo tiene muchos lugares que no son lugares seguros donde fallar, por lo que puedo ver que sería un desafío. Me pregunto si pensas en un taller como ese, como una especie de santuario del resto de la vida de las personas donde no pueden fallar, o si lo considerás más como enseñar a la gente sobre live coding como una herramienta para entender ese fracaso, en cualquier aspecto de la vida, ¿está bien? ¿O simplemente una idea restringida de algo que hemos inventado de estas narraciones que nos contamos?

Joanne Armitage:
Creo que está tratando de ubicar el fracaso como algo que construimos a través de nuestras expectativas, de nosotros mismos y de nuestro conocimiento. También es el fracaso de algo que es innatamente humano y cotidiano no mundano. Solo cosas como la creatividad, en realidad es super mundana y cotidiana, y humana. Como es el fracaso. Creo que se trata de un fracaso de reposicionamiento, en su mayoría... A menudo, siempre encuentro lo más importante para impulsar este tipo de narrativas en mis talleres feministas donde las mujeres me informaron que... en realidad, hice una serie de entrevistas con mujeres sobre su experiencia de aprender live coding, actualmente son programadoras en vivo que asistieron a los primeros talleres.

Joanne Armitage:
Una cosa que dejan muy claro es la actividad en estos talleres que pensaron que podían fallar fuera de la mirada masculina, y esas son sus palabras y su experiencia. Creo que se trata de ser capaz de fallar específicamente con la tecnología alrededor de las personas que te hacen sentir cómoda, y eso es lo que intentamos y hacemos. Creo que están cambiando las narrativas del fracaso tecnológico y cuántas de las mujeres que asisten a nuestros talleres lo han experimentado de una manera muy genérica. Eso responde tu pregunta?

Matthew Tift:
Sí, ni siquiera sé si hice una pregunta.

Joanne Armitage:
Creo que había algo allí.

Matthew Tift:
Sí, bueno, me recuerda algo que mi consejera de posgrado en la escuela solía decirme. Era una reconocida académica feminista de historia de la música, y me recordaba constantemente durante los nueve años que estuve en la escuela de posgrado que si no estamos hablando de género, raza o clase, ¿cuál es el punto? Tus descripciones de live coding, parece que hay mucho de eso y me pregunto si a veces simplemente lo piensas como una actividad divertida, o si es que tenés estas diferentes narrativas que, dependiendo de la situación, tal vez una está recibiendo más atención que la otra?

Joanne Armitage:
Sí, creo que es una pregunta muy interesante. Mientras lo pienso a través del tipo de lente más crítica, y eso a veces le quita un poco de alegría, pero en este momento creo que el pensamiento no es tan académico y la escritura en torno al live coding. Para mí, escribir no es tan fácil, así que lo encuentro bastante agotador. Y pensando. Pensar es más fácil, pero a veces articularlo a través del texto, irónicamente, es un poco más desafiante. Creo que me he comprometido un poco en la negociación, live coding como una práctica incorporada de género.

Joanne Armitage:
Estoy disfrutando un poco de la performance, un poco menos en este momento, pero creo que también... Estoy trabajando más, y lo encuentro realmente agotador. Estoy a la mitad de un semestre en este momento, y estoy lista para irme a la cama aproximadamente a las 8:00 p.m. Así que jugar con código el fin de semana es simplemente... ha sido muy difícil, y no he tenido muchos fines de semana libres. Así que sí, parte de la alegría no está muy presente en este momento, pero espero que el viaje a Japón la próxima semana me revitalice un poco en ese sentido. Creo que en un nivel más personal sobre lo que estoy tocando es que realmente debo comenzar a desafiarme para hacer algunas cosas más interesantes.

Joanne Armitage:
Estuve hablando con algunas de las personas que participaron en la gira el fin de semana, siento que todavía me estoy yendo del trabajo como lo hice hace 12 meses, y necesito hacer que pare para encontrar un sentido de nuevas formas de trabajo. Lo he estado tratando de hacer, pero realmente no me he centrado lo suficiente como para sentirme segura de hacerlo como una performance.

Matthew Tift:
Ciertamente, la noción de enseñar y corregir papers, y agregar fechas de live coding es mucho, como hacer malabares; especialmente cuando haces la mayoría de estos eventos, talleres y conciertos que haces. Así que te creo.

Joanne Armitage:
Joanne Armitage:
I know. Yeah. I have ... part of my contract isn't permanent, so I do feel the need to try and keep up as many of my external commitments as I can manage because I don't want to lose any work for the next ... In eight months, I don't really know where my work contract will be, so I don't really want to terminate in some way, so I am passing things on to other people, but some stuff is hard to resist. There's some quite cool things coming up that I don't know if I can announce. Actually I can announce that we're doing a panel at South by Southwest-

Lo sé. Sí. Tengo... parte de mi contrato no es permanente, por lo que siento la necesidad de tratar de cumplir con todos los compromisos externos que pueda manejar porque no quiero perder ningún trabajo para el próximo... los próximos ocho meses, realmente no sé en que quedará mi contrato de trabajo, así que realmente no quiero terminar de alguna manera, así que le estoy transmitiendo cosas a otras personas, pero es difícil resistir algunas cosas. Hay algunas cosas muy interesantes que no sé si puedo anunciar. En realidad puedo anunciar que estamos haciendo un panel en South by Southwest

Matthew Tift:
Oh, yo vi eso.

Joanne Armitage:
Es oficial

Matthew Tift:
Oh, vi la votación, pero no estaba seguro de si fue aceptado o no.

Joanne Armitage:
Sí, ha sido aceptado. Esperemos que haya otras cosas alrededor de eso, pero no estoy segura si puedo hablar de eso todavía. Pero sí, estamos muy entusiasmadas con eso.

Matthew Tift:
¿Eso es parte de la música South by Southwest o de la parte más tecnológica?

Joanne Armitage:
Creo que estaba trabajando en un festival de música en ese momento, así que pienso que solicité la pista tecnológica. Lo cual me sorprendió bastante haber hecho. Pensé que había puesto en música, pero tal vez nos encajaran.

Matthew Tift:
La parte de música, creo que es mucho más sobre la performance. Tal vez eso es lo que está haciendo también con... esa conferencia, he estado allí varias veces y es una de las conferencias de tecnología más grandes... no, tiene que ser la conferencia de tecnología más grande que he estado. Es tan grande, pero es un gran lugar para dar una charla porque generalmente van a ser miles de personas escuchando.

Joanne Armitage:
Sí, sí. Tenemos un... quiero decir, sé que estoy en el panel, así que simplemente me incluiré en esto, pero tenemos un panel realmente genial. Es Shelly Knotts, a quien he mencionado algunas veces: Alexandra Cárdenas, que ha trabajado tanto en la escena del live coding en América del Sur y América Central y actualmente reside en Berlín y Ciudad de México. Así que tiene esas experiencias increíbles que aportar, y viene de un gran... tiene un objetivo musical muy claro que realmente impulsa su trabajo. Su álbum es lanzado el sábado. Es como un minor plug. Alexandra Cárdenas; y Antonio Roberts, que también reside en el Reino Unido y es live coder visual. Creo que debería ser una discusión muy agradable.

Matthew Tift:
Eso suena genial. Recuperando algunas de estas preguntas sobre género y diversidad, he estado teniendo algunas conversaciones con un par de personas en mi comunidad local, y vamos a tener un grupo de TidalCycles, un grupo live coding esencialmente, y cuando veo algo del trabajo que estás haciendo me dan ganas de hacer lo mejor que puedo para asegurarte de que nuestro grupo sea lo más inclusivo y diverso y lo más acogedor posible. Puedo leer muchas palabras de moda en línea sobre ser aliados de género o crear un lugar donde las personas asuman una intención positiva o ese tipo de cosas. ¿Tenés alguna sugerencia para ayudarnos a que nuestro grupo de usuarios sea lo más acogedor posible?

Joanne Armitage:
Sí, realmente creo que la mejor manera de lidiar con la diversidad es encontrar personas con antecedentes que sean diferentes a los tuyos para darles posiciones de poder en tu grupo, ya sea sobre género, raza o clase. Eso es algo que es difícil de hacer cuando no hay ningún tipo de precedente, pero la forma de lidiar con eso es abrir un poco lo que estás haciendo para intentar incorporar mejor algunas experiencias diferentes. Una cosa que sucedió en el Reino Unido es que Alex McLean realmente presionó para incluir cosas como la creación de live coding y el pensamiento de live coding en manualidades, y trabajaba twiddlers y tejedores. Ya sea a través de la facilitación de la colaboración o... Creo que solo tratando de repensar lo que estás haciendo y cómo estás estructurando las cosas.

Joanne Armitage:
Creo que es realmente importante pensar y hacer que otras personas de diferentes orígenes lean y copien lo que has usado en eventos. Creo que la forma en que se lanza y se enmarca es realmente importante cuando los eventos se comparten. A veces... muchas de las personas con las que he hablado, y de las mujeres en particular con las que he hablado, lo vieron tocando conciertos. Vinieron a un taller y alguien dijo: "Oye, ¿por qué no tocás un espectáculo?" Y eso fue suficiente para convertirlos en una live coder, ¿sabes?

Matthew Tift:
Huh.

Joanne Armitage:
Entonces, a veces, solo leves provocaciones como: "Hola, ¿por qué no solo hablas sobre tu trabajo y podemos hablar de cómo puede incrustarse en lo que estamos haciendo?" Ese tipo de pequeños empujones, no es frecuente... Ya sabes, si el juego es de alguien no tenés que darles un gran empujón, solo un pequeño empujón, y la gente salta en ellos. Obviamente, hay otras personas en las que tenés que trabajar un poco, pero una vez que tienes un equipo diverso que organiza eventos, todo se vuelve un poco más fácil, me parece.

Matthew Tift:
Por supuesto. Sí, eso tiene mucho sentido, básicamente, solo tienes que modelar el comportamiento que intentas promover.

Joanne Armitage:
Si. Si.

Matthew Tift:
Después de haber asistido a tantos eventos de programación y grupos de usuarios locales, y grupos de programación donde se trata de pizzas y cervezas en algún estudio de diseño web moderno o algo así, y pensar cómo a algunos de los... les gusta la ubicación o simplemente tomar alcohol, puede cambiar toda la vibra en quien se siente bienvenido.

Joanne Armitage:
Si.

Matthew Tift:
He estado pensando algo sobre eso en términos de tipo estructurales de cosas.

Joanne Armitage:
Sí, eso también es muy importante, en realidad, porque el alcohol puede poner algo fuera de los límites para muchas personas diferentes. He escuchado a muchas personas que, incluso si beben, no iban a programar a eventos en bares debido a experiencias anteriores o negativas. También sé por experiencia que las personas a menudo no se sienten cómodas en instituciones como las universidades, y eso crea barreras para algunas personas, que es algo que doy por sentado por un momento como alguien que siempre ha estado en la universidad.

Joanne Armitage:
No todos se sienten cómodos en este espacio en el que trabajo. Así que sí, creo que tratar de encontrar y negociar espacios mutuos y probar cosas diferentes es realmente importante. Creo que tratar de llevar la codificación a espacios no convencionales y diferentes es una de las cosas más importantes que podría hacer como alguien interesado en el código y quién codifica. Creo que quién codifica es una pregunta realmente importante en sí misma.

Matthew Tift:
Sí, una de las otras personas que he tenido en el podcast, de un grupo llamado Asian Penguins, estos chicos de un distrito escolar hacen computadoras e instalan Linux en ellas, y luego las entregan a los niños de la comunidad que vienen de familias que no tienen suficiente para una computadora. Así que entregaron cientos de computadoras, y pensé que sería interesante colaborar de alguna manera con ellos o enseñarles live coding, o hacer algún tipo de taller para niños, pero sería bueno poder invitarlos a un encuentro donde esos niños pueden sentirse bienvenidos.

Matthew Tift:
Pero a veces simplemente cuando... como el horario o la falta de transporte para algunas personas, hay tantas razones por las que puedes excluir a la gente que puede ser algo intimidante si quieres intentarlo y pensar en todo eso. Una de las cosas que creo que es tan importante de tu trabajo es que sigues haciendo estos eventos y estos talleres, y son diferentes en diferentes lugares y ese tipo de cosas. Parece que estás haciendo un muy buen trabajo en ese sentido.

Joanne Armitage:
Oh, siempre podemos hacerlo mejor. Sí, el horario, cuidado de niños, cuánto cuesta llegar a alguna parte, son todos grandes factores que son muy fáciles de dar por sentado. Siento que a veces definitivamente doy eso por sentado. Normalmente hago talleres gratuitos, pero recordá que tengo uno que terminó costando 50 libras, no me di cuenta de que iba a ser mucho. Tratamos de hablar sobre estrategias para gestionar eso, especialmente porque va a ser un taller muy bueno. Son preguntas realmente importantes para hacerse, y creo que siempre se puede hacer mejor y la algorave necesita abordar algunas cuestiones relacionadas con raza y clase, y el tipo de diversidad de género fuera de lo binario.

Joanne Armitage:
Creo que no están pasando muchas cosas con respecto a esas cosas. Quiero decir, estamos haciendo algunas cosas como tratar de llegar a grupos subrepresentados, pero hice muchos talleres en el National Media Museum, que se encuentra en una ciudad llamada Bradford en el Reino Unido, no lejos de donde estoy en Leeds. Eso fue muy divertido, con chicos de 7 años de áreas socioeconómicas bajas de la ciudad. Súper divertido, pero es solo que tenés 15 minutos con ellos y luego ... y le di a muchísimas escuelas mi email, pero no creo que tengan los recursos, o el tiempo, o la capacidad para ir hacia adelante con este tipo de cosas.

Matthew Tift:
Suena que... ¿Solés aplicar a muchas becas?

Joanne Armitage:
Aplico para cualquier cosa yo misma, pero a menudo me agregan en becas de otras personas, o personas que se divierten y necesitan organizar un evento y creo que trabajar en este entorno de tecnología en vivo, súper creativo y experimental, divertido, y tonto si estás en el estado de ánimo adecuado, a veces te encuentras quizás más obvio... a la gente le gustará... una persona fácil de invitar. Pero pienso en el Reino Unido, donde tenemos mucho mejor acceso a financiación para este tipo de actividades que podría hacer en los Estados Unidos, solo por las cosas que he escuchado de otras personas.

Matthew Tift:
Por supuesto. Bueno, también suena como que sos parte de un grupo y he escuchado a personas decir cosas como "50 activistas que trabajan solas no son tan efectivas como 50 activistas que trabajan juntas".

Joanne Armitage:
Sí.

Matthew Tift:
Si tenés el deseo de enseñar a la gente, ayudar a la gente, dar la bienvenida a nuevas caras y ese tipo de cosas, ciertamente parece que el beneficio de trabajar juntos puede ser bastante efectivo.

Joanne Armitage:
Totalmente, y a la gente le gusta asumir diferentes roles. Algunas de las personas con las que trabajo son realmente buenas para organizar cosas y administrar la finalización de tareas, y yo soy mucho mejor para pensar en cosas, conceptualizar, generar ideas y la reunión real, y tocar, y trabajar en la enseñanza y la práctica. Me gusta mucho hacer cosas. Así que sí, es encontrar buenas personas con las que trabajar para equilibrar las fortalezas y debilidades de cada uno.

Joanne Armitage:
Definitivamente no soy como un actor solitario, y siento que trabajar con otras personas me da la energía para continuar con lo que estoy haciendo. Siento que podría estar haciendo más, pero creo que es solo porque en este momento estoy enseñando tanto que se convierte en la parte más ruidosa de mi vida y las otras cosas se convierten en algo como "Okey, fin de semana, prueba y trae toda la energía que puedas, y observa cuán convincente podés ser", y eso, ya sabés, lo obtuviste en conjunto.

Joanne Armitage:
Además, lo que me encanta del live coding es que coloca la programación en lo social y, como mencioné al comienzo de nuestra charla, para mí es a quién conocí a través de live coding, y una cosa que realmente me motiva a seguir y seguir haciendo conciertos. Es toda la gente increíble que conocés y te encontrás en el proceso. Eso es un gran privilegio.

Matthew Tift:
En efecto. Bueno, realmente aprecio que te hayas tomado el tiempo, una vez más, para venir al programa, Joanne. ¿Hay algo más que quisieras mencionar o agregar, o si alguien quisiera contactarte? ¿Querés compartir alguna información así?

Joanne Armitage:
En términos de agregados, hay una gira que estamos haciendo en Japón, así que si estás en algún lugar cerca de Osaka o Tokio, sería un placer venir y reunirme y saludarme. En email, para música, normalmente uso midigirl9090@gmail.com. Soy @joannnne, son cuatro Ns, @joannnne en Twitter. Sí, son las mejores maneras de contactarme. Si estás interesado en escuchar más... Estoy tratando de ser mejor en tener un sitio web para mostrar algunas de las cosas que hago y construyo, y algunos de mis textos.

Joanne Armitage:
Así que con suerte, cuando tenga un poco de tiempo en Navidad, voy a desarrollar eso un poco más. Tengo muchas ganas de compartir mis materiales de taller de forma más libre y abierta. Sí, quiero decir, hay tanto para hacer al archivarte y tu trabajo está ahí. Hay tanto involucrado en eso. Es algo más en la que no soy muy buena haciendo... y hay algunas personas que son realmente buenas en eso, y espero que pueda sacar algunas estrategias de ellos.

Matthew Tift:
Bien. Bueno, nuevamente muchas gracias por tu tiempo.

Joanne Armitage:
Gracias, Matthew. Saludos. Ha sido genial.

Matthew Tift:
Gracias por escuchar este episodio de Hacking Culture. Toda la música en este episodio es música de Joanne. Puede encontrar enlaces a ALGOBABEZ, OFFAL, el panel South by Southwest de Joanne y más en hackingculture.org/episode/18. Gracias por escuchar.

*Licencia Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.*
